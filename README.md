# deploy-app-engine

**Status: Beta**

> - `google_cloud_support_feature_flag` (Beta) flag needs to be enabled to use the Component.

The `deploy-app-engine` component deploys container images stored in Google Artifact Registry, or App Engine Flexible source code, to Google App Engine as part of your GitLab CI/CD pipeline.

## Prerequisites

* [Enable the App Engine API][enable-api].
* Set up a Google Cloud workload identity pool and provider by following the steps in the [GitLab Google Cloud integration onboarding process][onboarding].
* Set up service account impersonation to access Google Cloud resources by following the steps in [Allow your external workload to access Google Cloud resources][sa-impersonation].

## Billing

Usage of the `deploy-app-engine` GitLab component might incur Google Cloud billing charges depending on your usage. For more information on App Engine pricing, see [App Engine Pricing][app-engine-pricing].

## Usage

Add the following to your `.gitlab-ci.yml` file:

```yaml
include:
  - component: gitlab.com/google-gitlab-components/gae/deploy-app-engine@0.1.0
    inputs:
      project_id: "your-gcp-project-id"
      image_url: "us-central1-docker.pkg.dev/your-project/your-repo/your-image:latest"
      impersonated_service_account: "sa@your-gcp-project-id.iam.gserviceaccount.com"
      app_yaml: "app.yaml"
      promote: false
      version: "v1"
      bucket: "staging.your-gcp-project-id.appspot.com"
      stage: deploy
      as: staging
```

Specify the image URL if and only if you want to deploy a Docker image in the Artifact Registry. Otherwise, your source code in this repository will be zipped, uploaded to Cloud Storage and deployed to the App Engine Flexible environment.

### Inputs

| Input | Description | Example | Default Value |
|---|---|---|---|
| `project_id` | (Required) The Google Cloud project id | `my-project` |  |
| `image_url` | (Optional) The URL of the image to deploy. Docker URL must be from a valid Artifact Registry hostname. | `us-docker.pkg.dev/my-project/my-repo/my-image:latest` | |
| `impersonated_service_account` | (Required) A service account to impersonate for deployment. Provide the full email address. | `deploy-sa@my-project.iam.gserviceaccount.com` | |
| `app_yaml` | (Optional) The path to your `app.yaml` file within your repository. | `app.yaml` |  `app.yaml` |
| `promote` | (Optional) Whether to promote the deployed version to receive all traffic. | `false` | `true` |
| `version` | (Optional) A specific version to use for the deployment. Leave blank to auto-generate. | `v1` | (timestamp) |
| `bucket` | (Optional) A bucket to upload the source code zip to. | `app-staging` | `staging.${PROJECT_ID}.appspot.com` |
| `stage` | (Optional) The GitLab CI/CD stage where the component to be executed | `build` | `deploy` |
| `as` | (Optional) The name of the job to be execute | `deployment` |  `deploy-app-engine` |

## Authentication

Since Google App Engine doesn't support direct use of workload identity federation, the `deploy-app-engine` component uses service account impersonation to indirectly use workload identity federation.

### Set up service account impersonation

These instructions use the `gcloud` command-line tool.

To start using `gcloud`, install [gcloud][gcloud] locally.
Alternatively, use [Cloud Shell][cloud-shell].

1.  (Optional) Create a Google Cloud Service Account. If you already have a
    Service Account, take note of the email address and skip this step.

    ```sh
    # TODO: replace ${PROJECT_ID} with your value below.

    gcloud iam service-accounts create "my-service-account" \
      --project "${PROJECT_ID}"
    ```

2.  Get the full ID of the Workload Identity Pool you entered in GitLab IAM integration.

    ```sh
    # TODO: replace ${PROJECT_ID} with your value below.

    gcloud iam workload-identity-pools describe "gitlab" \
      --project="${PROJECT_ID}" \
      --location="global" \
      --format="value(name)"
    ```

3.  Allow authentications from the Workload Identity Pool to your Google Cloud
    Service Account.

    ```sh
    # TODO: replace ${PROJECT_ID}, ${WORKLOAD_IDENTITY_POOL_ID}, and ${GITLAB_PROJECT_ID}
    # with your values below.
    #
    # ${GITLAB_PROJECT_ID} can be obtained in the GitLab project settings.
    #
    # ${WORKLOAD_IDENTITY_POOL_ID} is the full pool ID.
    # For example: projects/123456789/locations/global/workloadIdentityPools/gitlab

    gcloud iam service-accounts add-iam-policy-binding "my-service-account@${PROJECT_ID}.iam.gserviceaccount.com" \
      --project="${PROJECT_ID}" \
      --role="roles/iam.workloadIdentityUser" \
      --member="principalSet://iam.googleapis.com/${WORKLOAD_IDENTITY_POOL_ID}/attribute.project_id/${GITLAB_PROJECT_ID}"
    ```

    For a complete list of OIDC custom claims, see [OIDC custom claims][oidc-custom-claims].

4.  Grant the Google Cloud service account permissions to access
    Google Cloud resources as needed. For a list of commonly required roles, see the [Authorization section](#authz).

<a name="authz" id="authz"></a>
## Authorization

To deploy to App Engine using service account impersonation, grant your service account the following IAM roles:

* **App Engine Admin (`roles/appengine.appAdmin`)**: provides full control over App Engine applications.
* **Artifact Registry Reader (`roles/artifactregistry.reader`)**: to view & get artifacts for implementing CI/CD pipeline.  
* **Service Account User (`roles/iam.serviceAccountUser`)**: to use the [default App Engine service account][app-engine-sa] to run deployment.
* **Storage Admin (`roles/storage.admin`)**: to upload files to Cloud Storage to store source artifacts, and to fetch build metadata from Cloud Storage.
* **Cloud Build Editor (`roles/cloudbuild.builds.editor`)**: to build the service.

You can grant these roles using the Google Cloud Console or `gcloud`.
For example, to grant the `roles/appengine.appAdmin` role to a service account:

```bash
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member=serviceAccount:${IMPERSONATED_SERVICE_ACCOUNT} \
    --role=roles/appengine.appAdmin
```

**Important:** 
* Replace the placeholders with your actual project ID, impersonated service account, and appropriate values.
* Ensure that you understand the [principle of least privilege][least-privilege] and grant only the necessary permissions to your service account. 

For a comprehensive introduction to granting roles, see [Manage access to projects, folders, and organizations][grant-roles].

## Troubleshooting

If you encounter any issues with the deployment, refer to the following resources:

* **App Engine Deployment Troubleshooting Guide:** [App Engine Deployment Troubleshooting Guide][app-engine-troubleshooting]
* **Google Cloud Workload Identity Federation Documentation:** [Google Cloud Workload Identity Federation Documentation][workload-identity-federation]

[enable-api]: https://console.cloud.google.com/apis/library/appengine.googleapis.com
[onboarding]: https://docs.gitlab.com/ee/integration/google_cloud_iam.html
[sa-impersonation]: https://cloud.google.com/iam/docs/workload-identity-federation-with-deployment-pipelines#access
[app-engine-pricing]: https://cloud.google.com/appengine/pricing
[gcloud]: https://cloud.google.com/sdk/docs/install
[cloud-shell]: https://cloud.google.com/shell/docs
[oidc-custom-claims]: https://docs.gitlab.com/ee/integration/google_cloud_iam.html#oidc-custom-claims
[app-engine-sa]: https://cloud.google.com/appengine/docs/flexible/configure-service-accounts#assign_an_app-level_default_service_account
[least-privilege]: https://cloud.google.com/iam/docs/using-iam-securely#least_privilege
[grant-roles]: https://cloud.google.com/iam/docs/granting-changing-revoking-access
[app-engine-troubleshooting]: https://cloud.google.com/appengine/docs/flexible/troubleshooting
[workload-identity-federation]: https://cloud.google.com/iam/docs/workload-identity-federation